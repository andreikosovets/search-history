// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDOxqT7i70cn7f4AyhTsPYZA9OOmkrx0QE',
    authDomain: 'search-history-2df1d.firebaseapp.com',
    databaseURL: 'https://search-history-2df1d.firebaseio.com',
    projectId: 'search-history-2df1d',
    storageBucket: 'search-history-2df1d.appspot.com',
    messagingSenderId: '470154541613'
  }
};
