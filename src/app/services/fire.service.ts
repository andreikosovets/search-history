import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Post } from '../interfaces/post';

@Injectable()
export class FireService {
  private itemsRef: AngularFireList<any>;
  posts: Observable<any[]>;

  constructor(db: AngularFireDatabase) {
    this.itemsRef = db.list('posts', ref => ref.orderByChild('date'));

    this.posts = this.itemsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).map(items => items.sort((a, b) => b.date - a.date));
  }

  removeItem(key: string) {
    return this.itemsRef.remove(key);
  }

  pushItem(post: Post) {
    return this.itemsRef.push(post);
  }
}
