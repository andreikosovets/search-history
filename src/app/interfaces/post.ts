export interface Post {
  text: string;
  date: number;
}
