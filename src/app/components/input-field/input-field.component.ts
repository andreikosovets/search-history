import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FireService } from '../../services/fire.service';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class InputFieldComponent implements OnInit {
  rForm: FormGroup;

  constructor(fb: FormBuilder, private fs: FireService) {
    /**
     * Set validators to reactive form
     * @type {FormGroup}
     */
    this.rForm = fb.group({
      'text' : [null, Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(100)])],
    });
  }

  ngOnInit() {
  }

  formSubmit(e, value) {
    e.preventDefault();
    if (value.text) {
      this.fs.pushItem({
        text: value.text,
        date: new Date().getTime(),
      });
      this.refreshControls();
    }
  }

  private refreshControls() {
    this.rForm.controls.text.markAsUntouched();
    this.rForm.controls.text.setValue(``);
  }
}
