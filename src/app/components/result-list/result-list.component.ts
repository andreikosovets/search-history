import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FireService } from '../../services/fire.service';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class ResultListComponent implements OnInit {
  items: Observable<any[]>;
  loading: boolean;
  itemsLength: number;

  constructor(private fs: FireService) {
    this.loading = true;

    /**
     * Get items from service
     */
    this.items = fs.posts;

    /**
     * Subscribe for show loading text, while getting remote data;
     */
    this.items.subscribe((data) => {
      this.itemsLength = data.length;
      this.loading = false;
    });
  }

  ngOnInit() {
  }

  removeItem(key: string) {
    return this.fs.removeItem(key);
  }
}
